#include <SPI.h>
#include <LoRa.h>

#define Button1 2
#define Button2 4
unsigned int ButtonState1;
unsigned int ButtonState2;
int start_flag;
unsigned int counter = 0;
String sendNum;
char sendNum_bin;
int counter_loop=0;
int sf;
void keyscan();
void setup() {
  start_flag=0;
  
  sf=11;
  pinMode(Button1,INPUT);
  pinMode(Button2,INPUT);
  
  sendNum_bin='?';
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Sender");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
    while(!start_flag)
  {
    Serial.print("sf: ");
    Serial.println(sf);
    
    keyscan();
    delay(500);
    }
  LoRa.setSpreadingFactor(sf);
  LoRa.setSignalBandwidth(125E3);
  LoRa.setCodingRate4(5);
  LoRa.setTxPower(20);
  LoRa.setSyncWord(0x34); 
  
  LoRa.disableCrc();
  sendNum="111111";

}

void loop() {
  Serial.print("Sending packet: ");
  Serial.println(counter);
  
  int numi=0;
  // send packet
  LoRa.beginPacket();
  if(counter==50|counter==100|counter==150|counter==200|counter==250|counter==300|counter==350|counter==400|counter==450)
  {
    sendNum=sendNum+"111111";
    counter_loop=counter/50;
  }
  for(numi=0;numi<=counter_loop;numi++)
  {
    LoRa.print(sendNum_bin);
    LoRa.print(sendNum_bin);
    }
  //LoRa.print(sendNum);
  LoRa.print("|");
  LoRa.print(counter);
  LoRa.endPacket();

  counter++;
  
  delay((300+counter_loop*300));
}
void keyscan() 
{
  
  if(digitalRead(Button1)==HIGH)
  {
      
          sf++;
          if(sf>12)
          sf=7;
          while(digitalRead(Button1));
        
    }
    if(digitalRead(Button2)==HIGH)
  {
      
          start_flag=1;
          while(digitalRead(Button2));
        
    }
    //Serial.println(ButtonState2);
  }
